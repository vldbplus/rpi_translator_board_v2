# RPI Translator Board V2

Here you can find the hardware files of the RPI Translator Board V2. This board 
is used to, among other things, transform the Raspberry PI 4 model B IO voltage 
levels (3.3V) to be able to drive the lpGBT IOs (1.2V).

Without the RPI Translator Board you will not be able to use the PiGBT web 
application which is used to control any lpGBT register and ease the
lpGBT testing and control.

# With the RPI Translator Board V2 + Raspberry Pi 4 Model B

You can use the whole PiGBT web application together with a VLDB+. You can
mainly:

- Communicate with the lpGBT through the I2C protocol.
- Drive the lpGBT MODE[3:0] configuration pins.
- Reset the lpGBT.
- Disable and enable the FEASTMPs housed on a VLDB+.
- Fuse the lpGBT housed on a VLDB+.

![RPI Translator Board V2](Docs/V2.png)